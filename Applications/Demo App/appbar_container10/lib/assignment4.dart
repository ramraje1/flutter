//Create 2 containers in the centre of the screen and give that color to the
//containers.

import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});
  @override
  State<Assignment4> createState() => _Assignment4State();
}

class _Assignment4State extends State<Assignment4> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: const Text("Assignment4")),
        actions: [
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.purple,
      ),
      body: Center(
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
            color: Color.fromARGB(255, 255, 149, 0),
            width: 360,
            height: 200,
          ),
          SizedBox(
            width: 80,
          ),
          Container(
            color: Color.fromARGB(255, 255, 149, 0),
            width: 360,
            height: 200,
          ),
        ]),
      ),
    );
  }
}
