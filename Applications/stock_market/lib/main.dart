import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'SignUpFormUi.dart';

void main() {
  runApp(SignUpPage());
}

// Define a custom widget for the sign up page
class SignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Return a scaffold widget with an app bar and a body
    return Scaffold(
      appBar: AppBar(
        title: Text('Sign Up'),
      ),
      body: SignUpForm(),
    );
  }
}

// class LoginPage extends StatefulWidget {
//   @override
//   _LoginPageState createState() => _LoginPageState();
// }

// class _LoginPageState extends State<LoginPage> {
//   final _emailController = TextEditingController();
//   final _passwordController = TextEditingController();

//   Future<void> _login() async {
//     final email = _emailController.text;
//     final password = _passwordController.text;

//     final response = await http.post(
//       Uri.parse('https://your-backend-url.com/login'),
//       body: json.encode({
//         'email': email,
//         'password': password,
//       }),
//       headers: {
//         'Content-Type': 'application/json',
//       },
//     );

//     if (response.statusCode == 200) {
//       // Login successful, navigate to the home screen
//       Navigator.pushReplacementNamed(context, '/home');
//     } else {
//       // Login failed, show an error message
//       ScaffoldMessenger.of(context).showSnackBar(
//         SnackBar(content: Text('Login failed')),
//       );
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Login'),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.all(16.0),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text('Email'),
//             TextField(
//               controller: _emailController,
//               decoration: InputDecoration(
//                 hintText: 'Enter your email',
//               ),
//             ),
//             SizedBox(height: 16.0),
//             Text('Password'),
//             TextField(
//               controller: _passwordController,
//               obscureText: true,
//               decoration: InputDecoration(
//                 hintText: 'Enter your password',
//               ),
//             ),
//             SizedBox(height: 16.0),
//             Center(
//               child: ElevatedButton(
//                 onPressed: _login,
//                 child: Text('Login'),
//               ),
//             ),
//             SizedBox(height: 16.0),
//             Center(
//               child: TextButton(
//                 onPressed: () {
//                   Navigator.pushReplacementNamed(context, '/signup');
//                 },
//                 child: Text('Sign up'),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
