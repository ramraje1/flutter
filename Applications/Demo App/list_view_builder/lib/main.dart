import 'package:flutter/material.dart';
import 'package:list_view_builder/ImageOnClick.dart';
import 'DisplayListOnCount.dart';
import 'imageList.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      //home: DisplayCount(),
      home: ImageList(),
      //home: ImageOnClick(),
      debugShowCheckedModeBanner: false,
    );
  }
}
