num x = 1;

void fun() {
  if (x <= 10) {
    print(x);
    x++;
    fun();
  }
}

void main() {
  fun();
}
