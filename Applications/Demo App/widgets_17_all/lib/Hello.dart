import 'package:flutter/material.dart';

class Widgets17 extends StatefulWidget {
  const Widgets17({super.key});
  @override
  State<Widgets17> createState() => _Widgets17State();
}

class _Widgets17State extends State<Widgets17> {
  int index = 0;
  int count = 1;
  List<String> images = [
    "https://c4.wallpaperflare.com/wallpaper/402/508/992/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/711/338/415/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/37/495/765/rolls-royce-dawn-rolls-royce-2018-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/538/280/3/rolls-royce-dawn-overdose-rolls-royce-2017-cars-hd-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/339/381/65/rolls-royce-dawn-overdose-rolls-royce-2017-cars-hd-wallpaper-preview.jpg"
  ];
  List<Widget> container = [];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title: const Text(
              'Youtube',
              style: TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.w700,
                  fontSize: 23),
            ),
            backgroundColor: Colors.white,
            actions: [
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.cast_connected_sharp,
                  color: Colors.black,
                ),
              ),
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.notifications_none_outlined,
                  color: Colors.black,
                ),
              ),
              IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.search,
                    color: Colors.black,
                  ))
            ],
          ),
          body: SingleChildScrollView(
            //  scrollDirection: Axis.vertical,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              const SizedBox(
                height: 5,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          TextButton(
                              onPressed: () {},
                              style: const ButtonStyle(
                                overlayColor:
                                    MaterialStatePropertyAll(Colors.black),
                                backgroundColor:
                                    MaterialStatePropertyAll(Colors.black26),
                              ),
                              child: const Text("All",
                                  style: TextStyle(color: Colors.white))),
                          const SizedBox(width: 5),
                          TextButton(
                              onPressed: () {},
                              style: const ButtonStyle(
                                  overlayColor:
                                      MaterialStatePropertyAll(Colors.black),
                                  backgroundColor:
                                      MaterialStatePropertyAll(Colors.black26)),
                              child: const Text("Mixes",
                                  style: TextStyle(color: Colors.white))),
                          const SizedBox(width: 5),
                          TextButton(
                              onPressed: () {},
                              style: const ButtonStyle(
                                  overlayColor:
                                      MaterialStatePropertyAll(Colors.black),
                                  backgroundColor:
                                      MaterialStatePropertyAll(Colors.black26)),
                              child: const Text("Music",
                                  style: TextStyle(color: Colors.white))),
                          const SizedBox(width: 5),
                          TextButton(
                              onPressed: () {},
                              style: const ButtonStyle(
                                  overlayColor:
                                      MaterialStatePropertyAll(Colors.black),
                                  backgroundColor:
                                      MaterialStatePropertyAll(Colors.black26)),
                              child: const Text("News",
                                  style: TextStyle(color: Colors.white))),
                          const SizedBox(width: 5),
                          TextButton(
                              onPressed: () {},
                              style: const ButtonStyle(
                                  overlayColor:
                                      MaterialStatePropertyAll(Colors.black),
                                  backgroundColor:
                                      MaterialStatePropertyAll(Colors.black26)),
                              child: const Text(
                                "Live",
                                style: TextStyle(color: Colors.white),
                              )),
                          const SizedBox(width: 5),
                          TextButton(
                              onPressed: () {},
                              style: const ButtonStyle(
                                  overlayColor:
                                      MaterialStatePropertyAll(Colors.black),
                                  backgroundColor:
                                      MaterialStatePropertyAll(Colors.black26)),
                              child: const Text(
                                "Sports",
                                style: TextStyle(color: Colors.white),
                              ))
                        ],
                      )),
                ],
              ),
              const SizedBox(
                height: 3,
              ),
              Column(
                children: [
                  Image.asset("assets/images/Dolby.jpg"),
                  const Text(
                    "Dolbywalya Song Making Video | Jaundya Na Balasaheb | Ajay Atul | Girish Kulkarni",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  ElevatedButton(onPressed: () {}, child: const Text("Play")),
                  Image.asset("assets/images/Gund.jpg"),
                  const Text(
                    "Pune Police Gangster Parade : पुणे पोलीस आयुक्तांची अॅक्शन, गुंडांची परेड",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  ElevatedButton(onPressed: () {}, child: const Text("Play")),
                  Image.asset("assets/images/Kayda.jpg"),
                  const Text(
                    "Uttarakhand UCC: Uttarakhand नंतर UCC कायदा, Gujarat, Assam, Maharashtra या राज्यात लागू होऊ शकतो ?",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  ElevatedButton(onPressed: () {}, child: const Text("Play")),
                  Image.asset("assets/images/Musafir.jpg"),
                  const Text(
                    "MUSAFIR HOON YARON 4k - Kishore Kumar HITS - Jeetendra - Parichay Movie Songs - R.D. Burman",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  ElevatedButton(onPressed: () {}, child: const Text("Play")),
                  Image.asset("assets/images/Shark.jpg"),
                  const Text(
                    "Shark Anupam ने 'Vecros' के Al Drone को किया Control | Shark Tank India S3 | Full Pitch",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                  ),
                  ElevatedButton(onPressed: () {}, child: const Text("Play")),
                ],
              )
            ]),
          )),
    );
  }
}
