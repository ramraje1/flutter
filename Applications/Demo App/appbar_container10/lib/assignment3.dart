//Create a screen with a deep purple app bar titled "Hello Core2web" and in the
//centre of the body create a blue container with (width: 360, height: 200).

import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});
  @override
  State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: const Text("Assignment3")),
        actions: [
          IconButton(
            icon: Icon(Icons.home),
            onPressed: () {},
          ),
          IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.purple,
      ),
      body: Center(
        child: Container(
          color: Colors.blue,
          width: 360,
          height: 200,
        ),
      ),
    );
  }
}
