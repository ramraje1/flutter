import 'package:flutter/material.dart';

class C2W extends StatefulWidget {
  const C2W({super.key});
  @override
  State<C2W> createState() => _C2WState();
}

class _C2WState extends State<C2W> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Core2web"),
          titleTextStyle: TextStyle(color: Colors.blueAccent, fontSize: 20),
          actions: [
            IconButton(
              iconSize: 40,
              icon: Icon(Icons.notifications),
              onPressed: () {},
            ),
            IconButton(
              iconSize: 40,
              icon: Icon(Icons.circle_rounded),
              onPressed: () {},
            ),
            IconButton(
              iconSize: 40,
              icon: Icon(Icons.settings),
              onPressed: () {},
            ),
          ],
          backgroundColor: Colors.black,
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.all(50),
          child: Container(
            child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              Container(
                width: 180,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.black54),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                            image: new ExactAssetImage(
                                "assets/images/student.jpg"),
                            fit: BoxFit.fill,
                          )),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Text("7000+"),
                        Text("Students")
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 50,
              ),
              Container(
                width: 180,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.black54),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                            image: new ExactAssetImage("assets/images/b.jpg"),
                            fit: BoxFit.fill,
                          )),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Text("100+"),
                        Text("Company")
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 50,
              ),
              Container(
                width: 180,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.black54),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                            image: new ExactAssetImage("assets/images/Bag.jpg"),
                            fit: BoxFit.fill,
                          )),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Text("5000+"),
                        Text("Placements")
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 50,
              ),
              Container(
                width: 180,
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.black54),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 70,
                      height: 70,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                            image:
                                new ExactAssetImage("assets/images/Google.jpg"),
                            fit: BoxFit.fill,
                          )),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Text("4000+"),
                        Text("Apps Downloads")
                      ],
                    )
                  ],
                ),
              ),
            ]),
          ),
        ));
  }
}
