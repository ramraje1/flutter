class Company {
  String? compName;
  String? loc;

  Company(this.compName, this.loc);

  void compInfo() {
    print(compName);
    print(loc);
  }
}

class Employee extends Company {
  int? empId;
  String? empName;

  Employee(this.empId, this.empName, String comp, String loc)
      : super(comp, loc);

  void empInfo() {
    print(empId);
    print(empName);
  }
}

void main() {
  Employee obj = new Employee(25, "Ramraje", "TCS", "Pune");
  obj.empInfo();
  obj.compInfo();
}
