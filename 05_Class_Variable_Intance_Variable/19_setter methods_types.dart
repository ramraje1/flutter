//setter methods
 class Demo {
  int? _x;
  String? str;
  double? _sal;

  Demo(this._x, this.str, this._sal);
/*
  void setX(int x) {
    _x = x;
  }

  void setName(String name) {
    str = name;
  }

  void setSal(double sal) {
    _sal = sal;
  }
*/
/*
  set setX(int x) {
    _x = x;
  }

  set setName(String name) {
    str = name;
  }

  set setSal(double sal) {
    _sal = sal;
  }
*/
  set setX(int x) => _x = x;
  set setName(String name) => str = name;
  set setSal(double sal) => _sal = sal;

  void info() {
    print(_x);
    print(str);
    print(_sal);
  }
}

void main() {
  Demo obj = new Demo(10, "Ramraje", 12.5);
  obj.info();

  // obj.setX(19);
  // obj.setName("Ramraje");
  // obj.setSal(99.9);
  // obj.info();

  obj.setX = 20;
  obj.setName = "Ram";
  obj.setSal = 25.5;
  obj.info();
}
