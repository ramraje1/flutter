import 'package:flutter/material.dart';

// A StatefulWidget that creates the symbol setting page
class Add_Edit_SymbolsPage extends StatefulWidget {
  const Add_Edit_SymbolsPage({Key? key}) : super(key: key);

  @override
  _Add_Edit_SymbolsPageState createState() => _Add_Edit_SymbolsPageState();
}

// A State object that holds the state of the symbol setting page
class _Add_Edit_SymbolsPageState extends State<Add_Edit_SymbolsPage> {
  // Declare the variables and controllers that you need to store the data and inputs of the symbol setting page
  String _chartInfo = 'Chart 1';
  String _exchange = "Exchange1";
  String _market = 'Market 1';
  String _productType = 'Product 1';
  String _orderType = 'Order 1';
  String _buySell = 'Buy';
  int _quantity = 0;
  double _target = 0.0;
  double _sl = 0.0;
  final TextEditingController _terminalSymbolController =
      TextEditingController();
  final TextEditingController _quantityController = TextEditingController();
  final TextEditingController _targetController = TextEditingController();
  final TextEditingController _slController = TextEditingController();
  final TextEditingController _symbolController = TextEditingController();

  // A method to handle the save button
  void _save() {
    // TODO: Add your save logic here, such as saving the settings or showing a confirmation message
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Add/Edit Symbol'),
        ),
        body: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Row(children: [
                          Text('Chart Info : '),
                          // Add a dropdown button for the chart info
                          DropdownButton<String>(
                            value: _chartInfo, // The selected value
                            items: const [
                              // The list of items, each containing a DropdownMenuItem widget
                              // You can use Text widgets to display the item values
                              DropdownMenuItem(
                                value: 'Chart 1',
                                child: Text('TradingView'),
                              ),
                              DropdownMenuItem(
                                value: 'Chart 2',
                                child: Text('Amibroker'),
                              ),
                            ],
                            onChanged: (value) {
                              // The logic to handle the change of the selected value
                              // You can use the setState method to update the state of the page
                              setState(() {
                                _chartInfo = value!;
                              });
                            },
                          ),
                        ]),
                        Row(children: [
                          Text('Exchange : '),
                          // Add a dropdown button for the chart info
                          DropdownButton<String>(
                            value: _exchange, // The selected value
                            items: const [
                              // The list of items, each containing a DropdownMenuItem widget
                              // You can use Text widgets to display the item values
                              DropdownMenuItem(
                                value: 'Exchange1',
                                child: Text('NSE'),
                              ),
                              DropdownMenuItem(
                                value: 'Exchange2',
                                child: Text('BSE'),
                              ),
                            ],
                            onChanged: (value) {
                              // The logic to handle the change of the selected value
                              // You can use the setState method to update the state of the page
                            },
                          ),
                        ]),
                        Row(
                          children: [
                            // Add a text widget for the market label
                            Text('Instrument Type : '),
                            // Add a dropdown button for the market
                            DropdownButton<String>(
                              value: _market, // The selected value
                              items: const [
                                // The list of items, each containing a DropdownMenuItem widget
                                // You can use Text widgets to display the item values
                                DropdownMenuItem(
                                  value: 'Market 1',
                                  child: Text('NFO'),
                                ),
                                DropdownMenuItem(
                                  value: 'Market 2',
                                  child: Text('EQTY'),
                                ),
                              ],
                              onChanged: (value) {
                                // The logic to handle the change of the selected value
                                // You can use the setState method to update the state of the page
                                setState(() {
                                  _market = value!;
                                });
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            // Add a text widget for the terminal symbol label
                            Text('Symbol : '),
                            // Add a text field for the terminal symbol input
                            Container(
                              width: 200,
                              height: 30,
                              child: TextField(
                                controller:
                                    _symbolController, // The text controller
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Row(
                          children: [
                            Text('Terminal Symbol : '),
                            // Add a text field for the terminal symbol input
                            Container(
                              width: 200,
                              height: 30,
                              child: TextField(
                                controller:
                                    _terminalSymbolController, // The text controller
                                decoration: const InputDecoration(
                                  border: OutlineInputBorder(),
                                ),
                              ),
                            ),
                          ],
                        ),
                        // Add a row of widgets for the terminal symbol and the product type
                        Row(
                          children: [
                            // Add a text widget for the product type label
                            Text('Product Type : '),
                            // Add a dropdown button for the product type
                            DropdownButton<String>(
                              value: _productType, // The selected value
                              items: const [
                                // The list of items, each containing a DropdownMenuItem widget
                                // You can use Text widgets to display the item values
                                DropdownMenuItem(
                                  value: 'Product 1',
                                  child: Text('MIS'),
                                ),
                                DropdownMenuItem(
                                  value: 'Product 2',
                                  child: Text('NRML'),
                                ),
                              ],
                              onChanged: (value) {
                                // The logic to handle the change of the selected value
                                // You can use the setState method to update the state of the page
                                setState(() {
                                  _productType = value!;
                                });
                              },
                            ),
                          ],
                        ),
                      ]),
                ),
                SizedBox(
                  width: 40,
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text('Order Type : '),
                          // Add a dropdown button for the order type
                          DropdownButton<String>(
                            value: _orderType, // The selected value
                            items: const [
                              // The list of items, each containing a DropdownMenuItem widget
                              // You can use Text widgets to display the item values
                              DropdownMenuItem(
                                value: 'Order 1',
                                child: Text('Market'),
                              ),
                              DropdownMenuItem(
                                value: 'Order 2',
                                child: Text('Limit'),
                              ),
                            ],
                            onChanged: (value) {
                              // The logic to handle the change of the selected value
                              // You can use the setState method to update the state of the page
                              setState(() {
                                _orderType = value!;
                              });
                            },
                          ),
                        ],
                      ),
                      // Add a row of widgets for the order type and the quantity
                      Row(
                        children: [
                          // Add a text widget for the order type label

                          // Add a text widget for the quantity label
                          Text('Quantity : '),
                          // Add a text field for the quantity input
                          Container(
                            width: 200,
                            height: 30,
                            child: TextField(
                              controller:
                                  _quantityController, // The text controller
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                              ),
                              keyboardType:
                                  TextInputType.number, // Only accept numbers
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          // Add a text widget for the buy/sell label
                          Text('Buy/Sell : '),
                          // Add a dropdown button for the buy/sell
                          DropdownButton<String>(
                            value: _buySell, // The selected value
                            items: const [
                              // The list of items, each containing a DropdownMenuItem widget
                              // You can use Text widgets to display the item values
                              DropdownMenuItem(
                                value: 'Buy',
                                child: Text('BUY'),
                              ),
                              DropdownMenuItem(
                                value: 'Sell',
                                child: Text('SELL'),
                              ),
                              DropdownMenuItem(
                                value: 'Both',
                                child: Text('BOTH'),
                              ),
                            ],
                            onChanged: (value) {
                              // The logic to handle the change of the selected value
                              // You can use the setState method to update the state of the page
                              setState(() {
                                _buySell = value!;
                              });
                            },
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          // Add a text widget for the target label
                          Text('Target : '),
                          // Add a text field for the target input
                          Container(
                            width: 200,
                            height: 30,
                            child: TextField(
                              controller:
                                  _targetController, // The text controller
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                              ),
                              keyboardType:
                                  TextInputType.number, // Only accept numbers
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        children: [
                          // Add a text widget for the sl label
                          Text('Stoploss : '),
                          // Add a text field for the sl input
                          Container(
                            width: 200,
                            height: 30,
                            child: TextField(
                              controller: _slController, // The text controller
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                              ),
                              keyboardType:
                                  TextInputType.number, // Only accept numbers
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),

            // Add an elevated button for the save button
            ElevatedButton(
              child: const Text('Save'),
              onPressed: _save,
              style: ElevatedButton.styleFrom(
                padding: EdgeInsets.all(16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
              ),
            ),
          ],
        ));
  }
}
