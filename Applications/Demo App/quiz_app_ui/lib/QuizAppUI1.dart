import 'package:flutter/material.dart';

class QuizAppUI1 extends StatefulWidget {
  const QuizAppUI1({super.key});
  @override
  State<QuizAppUI1> createState() => _QuizAppUI1State();
}

class _QuizAppUI1State extends State<QuizAppUI1> {
  int count = 1;
  int c1 = 0;
  Color set1() {
    if (c1 == 2 || c1 == 3 || c1 == 4) {
      return Colors.red;
    }
    return Colors.blue;
  }

  Color set2() {
    if (c1 == 1) {
      return Colors.green;
    } else if (c1 == 2 || c1 == 3 || c1 == 4) {
      return Colors.green;
    }
    return Colors.blue;
  }

  List questions = [
    "What is Flutter?",
    "What is Java?",
    "What is Python?",
    "What is AWS?",
    "What is SQL?"
  ];
  List options = [
    "FrameWork",
    "Language",
    "DataBase",
    "Server",
  ];
  String op1() {
    if (count == 1) {
      return options[0];
    } else if (count == 2) {
      return options[1];
    } else if (count == 3) {
      return options[1];
    } else if (count == 4) {
      return options[3];
    } else {
      return options[2];
    }
  }

  String Q2() {
    if (count == 1) {
      return questions[0];
    } else if (count == 2) {
      return questions[1];
    } else if (count == 3) {
      return questions[2];
    } else if (count == 4) {
      return questions[3];
    }
    return questions[4];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Quiz App",
          style: TextStyle(),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              const Padding(padding: EdgeInsets.all(10)),
              Container(
                child: Column(
                  children: [
                    Text(
                      "Question : $count/5",
                      style: const TextStyle(fontSize: 30),
                    ),
                    Text(
                      Q2(),
                      // "Question 1 : What is Flutter?",
                      style: const TextStyle(fontSize: 25),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(set2())),
                        onPressed: () {
                          setState(() {
                            c1 = 1;
                          });
                        },
                        child: Text(op1()
                            //"A.FrameWork",
                            )),
                    const SizedBox(
                      height: 16,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(set1())),
                        onPressed: () {
                          setState(() {
                            c1 = 2;
                          });
                        },
                        child: const Text("Language")),
                    const SizedBox(
                      height: 16,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(set1())),
                        onPressed: () {
                          setState(() {
                            c1 = 3;
                          });
                        },
                        child: const Text("DataBase")),
                    const SizedBox(
                      height: 16,
                    ),
                    ElevatedButton(
                        style: ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(set1())),
                        onPressed: () {
                          setState(() {
                            c1 = 4;
                          });
                        },
                        child: const Text("Server")),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            count++;
            c1 = 0;
            if (count == 6) {
              count = 1;
            }
          });
        },
        child: const Text("Next"),
      ),
    );
  }
}
