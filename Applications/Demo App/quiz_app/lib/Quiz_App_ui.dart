// import 'package:flutter/material.dart';

// class Quiz_AppUi extends StatefulWidget {
//   const Quiz_AppUi({super.key});
//   @override
//   State createState() => _Quiz_AppUiState();
// }

// class _Quiz_AppUiState extends State {
//   List<Map> allQuestions = [
//     {
//       "question": "Who is the founder of Microsoft?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "ElonMusk"],
//       "answerIndex": 2,
//     },
//     {
//       "question": "Who is the founder of Apple?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "ElonMusk"],
//       "answerIndex": 0,
//     },
//     {
//       "question": "Who is the founder of Amazon?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "ElonMusk"],
//       "answerIndex": 1,
//     },
//     {
//       "question": "Who is the founder of Tesla?",
//       "options": ["Steve Jobs", "Jeff Bezos", "Bill Gates", "ElonMusk"],
//       "answerIndex": 3,
//     },
//     {
//       "question": "Who is the founder of Google?",
//       "options": ["Steve Jobs", "Lary Page", "Bill Gates", "ElonMusk"],
//       "answerIndex": 1,
//     },
//   ];
//   bool questionScreen = true;
//   int questionIndex = 0;
//   int selectedIndex = -1;
//   bool isTrue = false;
//   int correctAnsIndex = -1;

//   Scaffold isQuestionScreen() {
//     if (questionScreen == true) {
//       return Scaffold(
//         appBar: AppBar(
//           title: const Text(
//             "QuizApp",
//             style: TextStyle(
//               fontSize: 30,
//               fontWeight: FontWeight.w800,
//               color: Colors.black,
//             ),
//           ),
//           centerTitle: true,
//           backgroundColor: Colors.white60,
//         ),
//         body: Column(
//           children: [
//             const SizedBox(
//               height: 25,
//             ),
//             Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 const Text(
//                   "Questions : ",
//                   style: TextStyle(
//                     fontSize: 25,
//                     fontWeight: FontWeight.w600,
//                   ),
//                 ),
//                 Text(
//                   "${questionIndex + 1}/${allQuestions.length}",
//                   style: const TextStyle(
//                     fontSize: 25,
//                     fontWeight: FontWeight.w600,
//                   ),
//                 ),
//               ],
//             ),
//             const SizedBox(
//               height: 50,
//             ),
//             SizedBox(
//               width: 380,
//               height: 50,
//               child: Text(
//                 allQuestions[questionIndex]["question"],
//                 style: const TextStyle(
//                   fontSize: 23,
//                   fontWeight: FontWeight.w400,
//                 ),
//               ),
//             ),
//             const SizedBox(
//               height: 30,
//             ),
//             ElevatedButton(
//               onPressed: () {
//                 setState(() {
//                   ansLogic(0);
//                 });
//               },
//               style: ButtonStyle(
//                   backgroundColor: MaterialStatePropertyAll(
//                     getButtonColor(0),
//                   ),
//                   fixedSize: const MaterialStatePropertyAll(
//                     Size(250, 40),
//                   ),
//                   alignment: Alignment.center),
//               child: Text(
//                 "A.${allQuestions[questionIndex]["options"][0]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.normal,
//                 ),
//               ),
//             ),
//             const SizedBox(
//               height: 20,
//             ),
//             ElevatedButton(
//               onPressed: () {
//                 setState(() {
//                   ansLogic(1);
//                 });
//               },
//               style: ButtonStyle(
//                   backgroundColor: MaterialStatePropertyAll(
//                     getButtonColor(1),
//                   ),
//                   fixedSize: const MaterialStatePropertyAll(
//                     Size(250, 40),
//                   ),
//                   alignment: Alignment.center),
//               child: Text(
//                 "B.${allQuestions[questionIndex]["options"][1]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.normal,
//                 ),
//               ),
//             ),
//             const SizedBox(
//               height: 20,
//             ),
//             ElevatedButton(
//               onPressed: () {
//                 setState(() {
//                   ansLogic(2);
//                 });
//               },
//               style: ButtonStyle(
//                   backgroundColor: MaterialStatePropertyAll(
//                     getButtonColor(2),
//                   ),
//                   fixedSize: const MaterialStatePropertyAll(
//                     Size(250, 40),
//                   ),
//                   alignment: Alignment.center),
//               child: Text(
//                 "C.${allQuestions[questionIndex]["options"][2]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.normal,
//                 ),
//               ),
//             ),
//             const SizedBox(
//               height: 20,
//             ),
//             ElevatedButton(
//               onPressed: () {
//                 setState(() {
//                   ansLogic(3);
//                 });
//               },
//               style: ButtonStyle(
//                   backgroundColor: MaterialStatePropertyAll(
//                     getButtonColor(3),
//                   ),
//                   fixedSize: const MaterialStatePropertyAll(
//                     Size(250, 40),
//                   ),
//                   alignment: Alignment.center),
//               child: Text(
//                 "D.${allQuestions[questionIndex]["options"][3]}",
//                 style: const TextStyle(
//                   fontSize: 20,
//                   fontWeight: FontWeight.normal,
//                 ),
//               ),
//             ),
//           ],
//         ),
//         floatingActionButton: FloatingActionButton(
//           onPressed: () {
//             setState(() {
//               if (questionIndex >= allQuestions.length - 1) {
//                 questionIndex = 0;
//               }
//               questionIndex++;
//             });
//           },
//           backgroundColor: Colors.grey,
//           child: const Icon(
//             Icons.forward,
//             color: Colors.black,
//           ),
//         ),
//       );
//     } else {
//       return const Scaffold();
//     }
//   }

//   Color? getButtonColor(int selectedIndex) {
//     if (isTrue && selectedIndex == this.selectedIndex) {
//       return Colors.green;
//     } else {
//       if (selectedIndex == correctAnsIndex) {
//         return Colors.green;
//       }
//       if (selectedIndex == this.selectedIndex && !isTrue) {
//         return Colors.red;
//       }
//     }
//   }

//   void ansLogic(int selectedIndex) {
//     correctAnsIndex = allQuestions[questionIndex]["answerIndex"];
//     this.selectedIndex = selectedIndex;
//     if (selectedIndex == correctAnsIndex) {
//       isTrue = true;
//     } else {
//       isTrue = false;
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return isQuestionScreen();
//   }
// }
