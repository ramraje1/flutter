class Company {
  int empCount = 500;
  String compName = "Google";
  String loc = "pune";

  void compInfo() {
    print(empCount);
    print(compName);
    print(loc);
  }
}

void main() {
  //Object1
  Company obj1 = new Company();
  obj1.compInfo();

  //Object2
  Company obj2 = Company();
  obj2.compInfo();

  //Object3
  new Company().compInfo();

  //Object4
  Company().compInfo();
}
