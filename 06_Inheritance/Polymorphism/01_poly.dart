class Parent {
  void career() {
    print("Engg.");
  }

  void marry() {
    print("XYZ");
  }
}

class Child extends Parent {
  void marry() {
    print("........");
  }

  void prof() {
    print("Software engg.");
  }
}

void main() {
  Child obj = new Child();
  obj.career();
  obj.marry();
  obj.prof();
}
