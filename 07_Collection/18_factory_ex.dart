abstract class Developer {
  factory Developer(String devType) {
    if (devType == "BackEnd")
      return BackEnd();
    else if (devType == "FrontEnd")
      return FrontEnd();
    else if (devType == "Mobile")
      return Mobile();
    else
      return Other();
  }
  void devLang();
}

class BackEnd implements Developer {
  void devLang() {
    print("NodeJs/SpringBoot");
  }
}

class FrontEnd implements Developer {
  void devLang() {
    print("React/Angular");
  }
}

class Mobile implements Developer {
  void devLang() {
    print("Flutter/Android/Kotlin");
  }
}

class Other implements Developer {
  void devLang() {
    print("Testing/Devops/Support");
  }
}

void main() {
  Developer obj1 = new Developer("FrontEnd");
  obj1.devLang(); // Output: React/Angular
  Developer obj2 = new Developer("BackEnd");
  obj2.devLang(); // Output: NodeJs/SpringBoot

  Developer obj3 = new Developer("Mobile");
  obj3.devLang(); // Output: Flutter/Android/Kotlin

  Developer obj4 = new Developer("Other");
  obj4.devLang(); // Output: Testing/Devops/Support
}
