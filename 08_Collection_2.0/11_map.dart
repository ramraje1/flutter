void main() {
  var player = {7: "MSD", 45: "Rohit", 18: "Virat"};
  print(player);
  print(player[7]);

  List<Map<List<String>, List<String>>> que = [
    {
      'question': 'What is the capital of India?',
      'options': ['Delhi', 'Mumbai', 'Bangalore', 'Hyderabad'],
    },
    {
      'question': 'Who is the current Prime Minister of India?',
      'options': [
        'Narendra Modi',
        'Jair Bolsonaro',
        'Ajay Devgn',
        'Sushma Swaraj'
      ]
    },
    {
      'question': 'Which one is not a country in the world?',
      'options': ['India', 'China', 'USA', 'Australia']
    }
  ];
  print(que[2][USA]);
}
