// import 'package:flutter/material.dart';
// import 'package:video_player/video_player.dart';

// class Widgets17 extends StatefulWidget {
//   const Widgets17({super.key});
//   @override
//   State<Widgets17> createState() => _Widgets17State();
// }

// class _Widgets17State extends State<Widgets17> {
//   int index = 0;
//   int count = 1;
//   List<String> images = [
//     "https://c4.wallpaperflare.com/wallpaper/402/508/992/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
//     "https://c4.wallpaperflare.com/wallpaper/711/338/415/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
//     "https://c4.wallpaperflare.com/wallpaper/37/495/765/rolls-royce-dawn-rolls-royce-2018-cars-4k-wallpaper-preview.jpg",
//     "https://c4.wallpaperflare.com/wallpaper/538/280/3/rolls-royce-dawn-overdose-rolls-royce-2017-cars-hd-wallpaper-preview.jpg",
//     "https://c4.wallpaperflare.com/wallpaper/339/381/65/rolls-royce-dawn-overdose-rolls-royce-2017-cars-hd-wallpaper-preview.jpg"
//   ];
//   List<Widget> container = [];

//   late VideoPlayerController _controller;
//   late Future<void> _initializeVideoPlayerFuture;

//   @override
//   void initState() {
//     super.initState();

//     // Create and store the VideoPlayerController. The VideoPlayerController
//     // offers several different constructors to play videos from assets, files,
//     // or the internet.
//     _controller = VideoPlayerController.networkUrl(
//       Uri.parse(
//         'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
//       ),
//     );

//     // Initialize the controller and store the Future for later use.
//     _initializeVideoPlayerFuture = _controller.initialize();

//     // Use the controller to loop the video.
//     _controller.setLooping(true);
//   }

//   @override
//   void dispose() {
//     // Ensure disposing of the VideoPlayerController to free up resources.
//     _controller.dispose();

//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//           appBar: AppBar(
//             title: const Text(
//               'Youtube',
//               style:
//                   TextStyle(color: Colors.black87, fontWeight: FontWeight.w700),
//             ),
//             backgroundColor: Colors.white,
//             actions: [
//               IconButton(
//                 onPressed: () {},
//                 icon: const Icon(
//                   Icons.cast_connected_sharp,
//                   color: Colors.black,
//                 ),
//               ),
//               IconButton(
//                 onPressed: () {},
//                 icon: const Icon(
//                   Icons.notifications_none_outlined,
//                   color: Colors.black,
//                 ),
//               ),
//               IconButton(
//                   onPressed: () {},
//                   icon: const Icon(
//                     Icons.search,
//                     color: Colors.black,
//                   ))
//             ],
//           ),
//           body: SingleChildScrollView(
//             //  scrollDirection: Axis.vertical,
//             child:
//                 Column(mainAxisAlignment: MainAxisAlignment.center, children: [
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 children: [
//                   SingleChildScrollView(
//                       scrollDirection: Axis.horizontal,
//                       child: Row(
//                         children: [
//                           TextButton(
//                               onPressed: () {},
//                               style: const ButtonStyle(
//                                 overlayColor:
//                                     MaterialStatePropertyAll(Colors.black),
//                                 backgroundColor:
//                                     MaterialStatePropertyAll(Colors.black26),
//                               ),
//                               child: const Text("All",
//                                   style: TextStyle(color: Colors.white))),
//                           const SizedBox(width: 5),
//                           TextButton(
//                               onPressed: () {},
//                               style: const ButtonStyle(
//                                   overlayColor:
//                                       MaterialStatePropertyAll(Colors.black),
//                                   backgroundColor:
//                                       MaterialStatePropertyAll(Colors.black26)),
//                               child: const Text("Mixes",
//                                   style: TextStyle(color: Colors.white))),
//                           const SizedBox(width: 5),
//                           TextButton(
//                               onPressed: () {},
//                               style: const ButtonStyle(
//                                   overlayColor:
//                                       MaterialStatePropertyAll(Colors.black),
//                                   backgroundColor:
//                                       MaterialStatePropertyAll(Colors.black26)),
//                               child: const Text("Music",
//                                   style: TextStyle(color: Colors.white))),
//                           const SizedBox(width: 5),
//                           TextButton(
//                               onPressed: () {},
//                               style: const ButtonStyle(
//                                   overlayColor:
//                                       MaterialStatePropertyAll(Colors.black),
//                                   backgroundColor:
//                                       MaterialStatePropertyAll(Colors.black26)),
//                               child: const Text("News",
//                                   style: TextStyle(color: Colors.white))),
//                           const SizedBox(width: 5),
//                           TextButton(
//                               onPressed: () {},
//                               style: const ButtonStyle(
//                                   overlayColor:
//                                       MaterialStatePropertyAll(Colors.black),
//                                   backgroundColor:
//                                       MaterialStatePropertyAll(Colors.black26)),
//                               child: const Text(
//                                 "Live",
//                                 style: TextStyle(color: Colors.white),
//                               )),
//                           const SizedBox(width: 5),
//                           TextButton(
//                               onPressed: () {},
//                               style: const ButtonStyle(
//                                   overlayColor:
//                                       MaterialStatePropertyAll(Colors.black),
//                                   backgroundColor:
//                                       MaterialStatePropertyAll(Colors.black26)),
//                               child: const Text(
//                                 "Sports",
//                                 style: TextStyle(color: Colors.white),
//                               )),
//                         ],
//                       )),
//                 ],
//               ),
//               Column(children: [
//                 FutureBuilder(
//                   future: _initializeVideoPlayerFuture,
//                   builder: (context, snapshot) {
//                     if (snapshot.connectionState == ConnectionState.done) {
//                       // If the VideoPlayerController has finished initialization, use
//                       // the data it provides to limit the aspect ratio of the video.
//                       return AspectRatio(
//                         aspectRatio: _controller.value.aspectRatio,
//                         // Use the VideoPlayer widget to display the video.
//                         child: VideoPlayer(_controller),
//                       );
//                     } else {
//                       // If the VideoPlayerController is still initializing, show a
//                       // loading spinner.
//                       return const Center(
//                         child: CircularProgressIndicator(),
//                       );
//                     }
//                   },
//                 ),
//                 ElevatedButton(
//                   onPressed: () {
//                     // Wrap the play or pause in a call to `setState`. This ensures the
//                     // correct icon is shown.
//                     setState(() {
//                       // If the video is playing, pause it.
//                       if (_controller.value.isPlaying) {
//                         _controller.pause();
//                       } else {
//                         // If the video is paused, play it.
//                         _controller.play();
//                       }
//                     });
//                   },
//                   child: Text("Play"),
//                   // child: Icon(
//                   //   Icons.play_circle,
//                   //   color: Colors.black,
//                   //   size: 20,
//                   // )
//                 )
//               ]),
//               Column(children: [
//                 FutureBuilder(
//                   future: _initializeVideoPlayerFuture,
//                   builder: (context, snapshot) {
//                     if (snapshot.connectionState == ConnectionState.done) {
//                       // If the VideoPlayerController has finished initialization, use
//                       // the data it provides to limit the aspect ratio of the video.
//                       return AspectRatio(
//                         aspectRatio: _controller.value.aspectRatio,
//                         // Use the VideoPlayer widget to display the video.
//                         child: VideoPlayer(_controller),
//                       );
//                     } else {
//                       // If the VideoPlayerController is still initializing, show a
//                       // loading spinner.
//                       return const Center(
//                         child: CircularProgressIndicator(),
//                       );
//                     }
//                   },
//                 ),
//                 ElevatedButton(
//                   onPressed: () {
//                     // Wrap the play or pause in a call to `setState`. This ensures the
//                     // correct icon is shown.
//                     setState(() {
//                       // If the video is playing, pause it.
//                       if (_controller.value.isPlaying) {
//                         _controller.pause();
//                       } else {
//                         // If the video is paused, play it.
//                         _controller.play();
//                       }
//                     });
//                   },
//                   child: Text("Play"),
//                   // child: Icon(
//                   //   Icons.play_circle,
//                   //   color: Colors.black,
//                   //   size: 20,
//                   // )
//                 )
//               ]),
//               Column(children: [
//                 FutureBuilder(
//                   future: _initializeVideoPlayerFuture,
//                   builder: (context, snapshot) {
//                     if (snapshot.connectionState == ConnectionState.done) {
//                       // If the VideoPlayerController has finished initialization, use
//                       // the data it provides to limit the aspect ratio of the video.
//                       return AspectRatio(
//                         aspectRatio: _controller.value.aspectRatio,
//                         // Use the VideoPlayer widget to display the video.
//                         child: VideoPlayer(_controller),
//                       );
//                     } else {
//                       // If the VideoPlayerController is still initializing, show a
//                       // loading spinner.
//                       return const Center(
//                         child: CircularProgressIndicator(),
//                       );
//                     }
//                   },
//                 ),
//                 Text("Butterfly Video"),
//                 ElevatedButton(
//                   onPressed: () {
//                     // Wrap the play or pause in a call to `setState`. This ensures the
//                     // correct icon is shown.
//                     setState(() {
//                       // If the video is playing, pause it.
//                       if (_controller.value.isPlaying) {
//                         _controller.pause();
//                       } else {
//                         // If the video is paused, play it.
//                         _controller.play();
//                       }
//                     });
//                   },
//                   child: Text("Play"),
//                   // child: Icon(
//                   //   Icons.play_circle,
//                   //   color: Colors.black,
//                   //   size: 20,
//                   // )
//                 )
//               ])
//             ]),
//           )),
//     );
//   }
// }
