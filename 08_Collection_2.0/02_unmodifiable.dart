void main() {
  List player1 = ["Rohit", "Shubman", "Virat", "KLRahul"];
  List player2 = List.unmodifiable(player1);
  print("Player 1: $player1");
  print("Player 2: $player2");

  player1[0] = "Rohit Sharma";

  print("Player 1: $player1");
  print("Player 2: $player2");

  player2[1] = "Sara";

  print("Player 1: $player1");
  print("Player 2: $player2");
}
