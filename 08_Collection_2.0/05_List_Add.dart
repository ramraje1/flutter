void main() {
  var progLang = List.empty(growable: true);

  progLang.add("Cpp");
  progLang.add("Java");
  progLang.add("Python");
  progLang.add("Java");

  var lang = ["ReactJS", "SpringBoot", "Flutter"];

//addAll
  progLang.addAll(lang);
  print(progLang); // [Cpp, Java, Python, Java, ReactJS, SpringBoot, Flutter]

  //insert
  progLang.insertAll(3, ["Go ", "Swift"]);
  print(
      progLang); //[Cpp, Java, Python, Go , Swift, Java, ReactJS, SpringBoot, Flutter]

  //replaceRange
  //progLang.replaceRange(3, 7, "Dart");
  print(progLang);
}
