import 'package:automatic_trading/LoginPage.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';

class StockHomePage extends StatefulWidget {
  @override
  State<StockHomePage> createState() => _HomePage();
}

class _HomePage extends State<StockHomePage> {
  final colorizeColors = [
    Colors.lightGreenAccent,
    Colors.blue,
    Colors.yellow,
    Colors.red,
  ];

  final colorizeTextStyle = TextStyle(
    fontSize: 50.0,
    fontFamily: 'Horizon',
  );
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Stack(
          fit: StackFit.expand,
          children: [
            Image.asset(
              'assets/images/algo.jpg', // Replace 'background_image.jpg' with your image path
              fit: BoxFit.cover,
            ),
            Padding(padding: EdgeInsets.fromLTRB(100, 100, 100, 0)),
            Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const LoginPage()));
                    },
                    // child: Text(
                    //   'Get Started',
                    //   style: TextStyle(
                    //       color: Colors.white70,
                    //       fontSize: 20,
                    //       fontWeight: FontWeight.w600),
                    // ),
                    child: DefaultTextStyle(
                      style: const TextStyle(
                          fontSize: 20.0,
                          // fontFamily: 'Agne',
                          fontWeight: FontWeight.w700),
                      child: AnimatedTextKit(
                        pause: Duration(seconds: 2),
                        stopPauseOnTap: true,
                        isRepeatingAnimation: true,
                        // totalRepeatCount: 100,
                        repeatForever: true,
                        animatedTexts: [
                          TypewriterAnimatedText('Get Started'),
                        ],
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => const LoginPage()));
                        },
                      ),
                    ),
                    // child: TextLiquidFill(
                    //   text: 'Get Started',
                    //   waveColor: Colors.green,
                    //   // boxBackgroundColor: Colors.redAccent,
                    //   textStyle: TextStyle(
                    //       //color: Colors.white70,
                    //       fontSize: 20,
                    //       fontWeight: FontWeight.w600),
                    // ),
                  ),
                ]),
            Center(
              child: Column(
                children: [
                  SizedBox(
                    height: 160,
                  ),
// animated Text
                  DefaultTextStyle(
                    style: const TextStyle(
                        fontSize: 50.0,
                        // fontFamily: 'Agne',
                        fontWeight: FontWeight.w700),
                    child: AnimatedTextKit(
                      stopPauseOnTap: true,
                      isRepeatingAnimation: true,
                      // totalRepeatCount: 100,
                      repeatForever: true,
                      animatedTexts: [
                        // TypewriterAnimatedText('Automatic Trading'),
                        ColorizeAnimatedText(
                          'Automatic Trading',
                          textStyle: colorizeTextStyle,
                          colors: colorizeColors,
                        ),
                        // WavyAnimatedText('Automatic Trading'),
                      ],
                      onTap: () {
                        print("Tap Event");
                      },
                    ),
                  )

                  // Text(
                  //   "Automatic",
                  //   style: TextStyle(
                  //       fontWeight: FontWeight.w800,
                  //       fontSize: 50,
                  //       color: Colors.white),
                  // ),
                  // Text(
                  //   "Trading",
                  //   style: TextStyle(
                  //       fontWeight: FontWeight.w800,
                  //       fontSize: 35,
                  //       color: Colors.white),
                  // ),

                  //       SizedBox(
                  //         height: 70,
                  //       ),
                  //       SizedBox(
                  //         width: 400,
                  //         child: Text(
                  //           "             The key to trading success                is emotional discipline. If intelligence were the key, there would be a lot more people       making money trading",
                  //           style: TextStyle(
                  //               fontSize: 20,
                  //               fontWeight: FontWeight.w500,
                  //               color: Colors.white),
                  //         ),
                  //       )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
