import 'package:automatic_trading/DetailsPage.dart';
import 'package:flutter/material.dart';
import 'SignUpPage.dart';
import 'ForgotPassword.dart';

// A StatefulWidget that creates the login page
class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

// A State object that holds the state of the login page
class _LoginPageState extends State<LoginPage> {
  // Text controllers for the text fields
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  // A form key to access and validate the form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // A bool variable to indicate whether the login process is in progress or not
  bool _isLoading = false;

  // A method to handle the login logic
  void _login() async {
    // Validate the form
    if (_formKey.currentState!.validate()) {
      // Set the loading indicator to true
      setState(() {
        _isLoading = true;
      });
      // Get the input values
      String email = _emailController.text.trim();
      String password = _passwordController.text.trim();
      // TODO: Add your login logic here, such as authenticating the user and navigating to the home page
      // For example, you can use Firebase Authentication to sign in the user
      // try {
      //   UserCredential userCredential =
      //       await FirebaseAuth.instance.signInWithEmailAndPassword(
      //     email: email,
      //     password: password,
      //   );
      //   // Navigate to the home page
      //   Navigator.pushReplacement(
      //       context, MaterialPageRoute(builder: (context) => DetailsPage()));
      // } on FirebaseAuthException catch (e) {
      //   // Handle the login error
      //   // For example, you can show a snackbar with the error message
      //   ScaffoldMessenger.of(context)
      //       .showSnackBar(SnackBar(content: Text(e.message!)));
      // }
      // Set the loading indicator to false
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text('Login'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              const SizedBox(height: 16),
              const Text(
                'LOGIN',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.all(16),
                child: TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                      labelText: 'Email',
                      labelStyle: TextStyle(color: Colors.black),
                      border: OutlineInputBorder(),
                      fillColor: Colors.black),
                  validator: (value) {
                    // Validate the email input
                    if (value!.isEmpty) {
                      return 'Enter your email';
                    }
                    if (!value.contains('@')) {
                      return 'Enter a valid email';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: TextFormField(
                  controller: _passwordController,
                  decoration: const InputDecoration(
                    labelText: 'Password',
                    labelStyle: TextStyle(color: Colors.black),
                    border: OutlineInputBorder(),
                  ),
                  obscureText: true,
                  validator: (value) {
                    // Validate the password input
                    if (value!.isEmpty) {
                      return 'Please enter your password';
                    }
                    if (value.length < 6) {
                      return 'Please enter a password with at least 6 characters';
                    }
                    return null;
                  },
                ),
              ),
              TextButton(
                onPressed: () {
                  // Navigate to the forgot password page
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ForgotPasswordPage()));
                },
                child: const Text('Forgot Password',
                    style: TextStyle(color: Color.fromARGB(255, 82, 176, 252))),
              ),
              Container(
                height: 50,
                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                child: _isLoading
                    ? const CircularProgressIndicator()
                    : ElevatedButton(
                        child: const Text('Login'),
                        onPressed: () {
                          // Navigate to the details page
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => DetailsPage()));
                        },
                      ),
              ),
              Row(
                // ignore: sort_child_properties_last
                children: <Widget>[
                  Padding(padding: EdgeInsets.fromLTRB(0, 30, 0, 30)),
                  const Text(
                    'Don\'t have an account ?',
                    style: TextStyle(color: Colors.black),
                  ),
                  TextButton(
                    child: const Text(
                      'Create Account',
                      style: TextStyle(
                          fontSize: 20,
                          color: Color.fromARGB(255, 82, 176, 252)),
                    ),
                    onPressed: () {
                      // Navigate to the create account page
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const CreateAccountPage()));
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
