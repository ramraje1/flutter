import 'package:flutter/material.dart';
import 'Youtube.dart';
import 'Hello.dart';
import 'Video.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      //home: Youtube()
      home: Widgets17(),
    );
  }
}
