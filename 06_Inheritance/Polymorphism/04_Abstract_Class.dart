abstract class parent {
  void property() {
    print("Gold,Bunglow,Flats,Cars,Bikes");
  }

  void carrer();

  void marry();
}

class Child extends parent {
  void carrer() {
    print("Engg.");
  }

  void marry() {
    print("Selena");
  }
}

void main() {
  parent obj = new Child();

  obj.property();
  obj.carrer();
  obj.marry();
}
