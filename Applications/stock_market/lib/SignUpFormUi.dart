// Import the material package to use material widgets
import 'package:flutter/material.dart';

// Define a custom widget for the sign up form
class SignUpForm extends StatefulWidget {
  // Create a global key to identify the form
  final _formKey = GlobalKey<FormState>();

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

// Define the state of the sign up form widget
class _SignUpFormState extends State<SignUpForm> {
  // Define the controllers for the text fields
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  // Define a function to validate and submit the form
  void _submitForm() {
    // Check if the form is valid
    if (widget._formKey.currentState!.validate()) {
      // Get the values from the controllers
      String name = _nameController.text;
      String email = _emailController.text;
      String password = _passwordController.text;
      // TODO: Add your logic to sign up the user with the given values
      // For example, you can use Firebase Authentication service
      // Show a success message
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Sign up successful!')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // Return a form widget with four text fields and a button
    return Form(
      key: widget._formKey,
      child: Column(
        children: [
          // Name field
          TextFormField(
            controller: _nameController,
            decoration: InputDecoration(
              labelText: 'Name',
              icon: Icon(Icons.person),
            ),
            validator: (value) {
              // Check if the value is empty
              if (value!.isEmpty) {
                return 'Please enter your name';
              }
              return null;
            },
          ),
          // Email field
          TextFormField(
            controller: _emailController,
            decoration: InputDecoration(
              labelText: 'Email',
              icon: Icon(Icons.email),
            ),
            validator: (value) {
              // Check if the value is empty or not a valid email
              if (value!.isEmpty || !value!.contains('@')) {
                return 'Please enter a valid email';
              }
              return null;
            },
          ),
          // Password field
          TextFormField(
            controller: _passwordController,
            decoration: InputDecoration(
              labelText: 'Password',
              icon: Icon(Icons.lock),
            ),
            obscureText: true,
            validator: (value) {
              // Check if the value is empty or less than 8 characters
              if (value!.isEmpty || value.length < 8) {
                return 'Please enter a password with at least 8 characters';
              }
              return null;
            },
          ),
          // Confirm password field
          TextFormField(
            controller: _confirmPasswordController,
            decoration: InputDecoration(
              labelText: 'Confirm Password',
              icon: Icon(Icons.lock),
            ),
            obscureText: true,
            validator: (value) {
              // Check if the value is empty or not equal to the password
              if (value!.isEmpty || value != _passwordController.text) {
                return 'Please enter the same password';
              }
              return null;
            },
          ),
          // Sign up button
          ElevatedButton(
            onPressed: _submitForm,
            child: Text('Sign Up'),
          ),
        ],
      ),
    );
  }
}
