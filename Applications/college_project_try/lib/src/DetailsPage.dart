import 'package:flutter/material.dart';

import 'ClosedOrder.dart';
import 'OrderPage.dart';
import 'Settings.dart';
import 'SymbolSetting.dart';

// A StatefulWidget that creates the details page
class DetailsPage extends StatefulWidget {
  const DetailsPage({Key? key}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

// A State object that holds the state of the details page
class _DetailsPageState extends State<DetailsPage> {
  // A bool variable to indicate whether the Order Page is visible or not
  bool _showOrderPage = false;
  bool _showSymbolSettingPage = false;
  bool _showClosedOrderPage = false;
  bool _showSettingPage = false;

  // A method to handle the order button
  void _order() {
    // Toggle the visibility of the Order Page
    setState(() {
      _showOrderPage = !_showOrderPage;
      _showSymbolSettingPage = false;
      _showClosedOrderPage = false;
    });
  }

  void _symbolSetting() {
    setState(() {
      _showSymbolSettingPage = !_showSymbolSettingPage;
      _showOrderPage = false;
      _showClosedOrderPage = false;
      _showSettingPage = false;
    });
  }

  // A method to handle the closed orders button
  void _closedOrders() {
    // TODO: Add your closed orders logic here, such as showing a list of closed orders or navigating to another page
    setState(() {
      _showClosedOrderPage = !_showClosedOrderPage;
      _showSymbolSettingPage = false;
      _showOrderPage = false;
      _showSettingPage = false;
    });
  }

  // A method to handle the settings button
  void _settings() {
    setState(() {
      _showSettingPage = !_showSettingPage;
      _showSymbolSettingPage = false;
      _showOrderPage = false;
      _showClosedOrderPage = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('DETAILS'),
        centerTitle: true,
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 20),
        children: [
          // SizedBox(
          //   height: 20,
          // ),

          // Add a row of elevated buttons
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                child: const Text('Open Order'),
                onPressed: _order,
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
              ),
              ElevatedButton(
                child: const Text('Symbol Settings'),
                onPressed: _symbolSetting,
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
              ),
              ElevatedButton(
                child: const Text('Closed Orders'),
                onPressed: _closedOrders,
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
              ),
              ElevatedButton(
                child: const Text('Settings'),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SettingPage()));
                },
                style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.all(16),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20)),
                ),
              ),
            ],
          ),
          // Add a visibility widget to show or hide the Order Page
          Visibility(
            visible: _showOrderPage,
            child: OrderPage(),
          ),
          Visibility(
            visible: _showClosedOrderPage,
            child: ClosedOrderPage(),
          ),
          Visibility(
            visible: _showSymbolSettingPage,
            child: SymbolSettingsPage(),
          ),
        ],
      ),
    );
  }
}
