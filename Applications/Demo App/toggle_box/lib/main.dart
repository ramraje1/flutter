import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "Toggle Box",
      debugShowCheckedModeBanner: false,
      home: ToggleBox(),
    );
  }
}

class ToggleBox extends StatefulWidget {
  const ToggleBox({super.key});
  @override
  State<ToggleBox> createState() => _ToggleBoxState();
}

class _ToggleBoxState extends State<ToggleBox> {
  int count1 = 0;
  int count2 = 0;

  Color setColorBox1() {
    if (count1 == 0) {
      return Colors.red;
    } else if (count1 == 1) {
      return Colors.orange;
    } else if (count1 == 2) {
      return Colors.grey;
    } else {
      count1 = 0;
      return Colors.black;
    }
  }

  Color setColorBox2() {
    if (count2 == 0) {
      return Colors.red;
    } else if (count2 == 1) {
      return Colors.orange;
    } else if (count2 == 2) {
      return Colors.grey;
    } else {
      count2 = 0;
      return Colors.black;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("TOGGLE BOX"),
        centerTitle: true,
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(width: 100, height: 100, color: setColorBox1()),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      count1++;
                    });
                  },
                  child: const Text("Button 1"))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 100,
                height: 100,
                color: setColorBox2(),
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      count2++;
                    });
                  },
                  child: const Text("Button 2"))
            ],
          )
        ],
      ),
    );
  }
}
