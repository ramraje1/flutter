class Parent {
  int x = 10;
  String str1 = "Parent Name";

  void parentMethod() {
    print(x);
    print(str1);
  }
}

class Child extends Parent {
  int x = 20;
  String str1 = "Data";
  void ChildMethod() {
    print(x);
    print(str1);
  }
}

void main() {
  Child obj = new Child();
  Parent obj1 = new Parent();
  Parent obj2 = new Child();
  print(obj.x);
  print(obj.str1);
  obj.parentMethod();

  obj1.parentMethod();
  obj2.parentMethod();
}
