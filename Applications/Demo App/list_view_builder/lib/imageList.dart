import 'package:flutter/material.dart';

class ImageList extends StatefulWidget {
  const ImageList({super.key});
  @override
  State createState() => _ImageListState();
}

class _ImageListState extends State<ImageList> {
  List<String> imageList = [
    "https://c4.wallpaperflare.com/wallpaper/402/508/992/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/711/338/415/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/37/495/765/rolls-royce-dawn-rolls-royce-2018-cars-4k-wallpaper-preview.jpg"
  ];
  int index = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(centerTitle: true, title: Text("Display Image")),
      body: ListView.builder(
          itemCount: imageList.length,
          itemBuilder: (context, index) {
            return Container(
                width: 600,
                alignment: Alignment.centerLeft,
                color: Colors.lime,
                child: Image.network(imageList[index]));
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {});
        },
        child: const Icon(Icons.add),
      ),
    ));
  }
}
