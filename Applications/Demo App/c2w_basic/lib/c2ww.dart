import 'package:flutter/material.dart';

class C2Ww extends StatefulWidget {
  const C2Ww({super.key});
  @override
  State<C2Ww> createState() => _C2WwState();
}

class _C2WwState extends State<C2Ww> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Core2web"),
        titleTextStyle: TextStyle(color: Colors.blueAccent, fontSize: 20),
        actions: [
          IconButton(
            iconSize: 40,
            icon: Icon(Icons.notifications),
            onPressed: () {},
          ),
          IconButton(
            iconSize: 40,
            icon: Icon(Icons.circle_rounded),
            onPressed: () {},
          ),
          IconButton(
            iconSize: 40,
            icon: Icon(Icons.settings),
            onPressed: () {},
          ),
        ],
        backgroundColor: Colors.black,
      ),
      body: Container(
        child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
              Image.network(
                  "https://t3.ftcdn.net/jpg/03/31/59/64/360_F_331596496_IBES7GleA7yyj4GCRnbXTsHu0SdJW40f.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://t3.ftcdn.net/jpg/03/31/59/64/360_F_331596496_IBES7GleA7yyj4GCRnbXTsHu0SdJW40f.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://e0.pxfuel.com/wallpapers/1009/824/desktop-wallpaper-ram-mandir-ram-mandir-ayodhya-for-thumbnail.jpg"),
            ])),
      ),
    );
  }
}
