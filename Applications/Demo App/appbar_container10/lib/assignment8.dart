// In the screen, add a container with size (width: 300,height:300). Also, add
// a border to that Container, the border must be of color red.

import 'package:flutter/material.dart';

class Assignment8 extends StatefulWidget {
  const Assignment8({super.key});
  @override
  State<Assignment8> createState() => _Assignment8State();
}

class _Assignment8State extends State<Assignment8> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
              child: const Text(
            'Assignment8',
            style: TextStyle(color: Colors.black),
          )),
          actions: [
            IconButton(
              color: Colors.black,
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            IconButton(
              color: Colors.black,
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
          ],
          backgroundColor: Color.fromARGB(255, 254, 119, 0),
        ),
        body: Center(
          child: Container(
            width: 300,
            height: 300,
            decoration:
                BoxDecoration(border: Border.all(color: Colors.red, width: 10)),
          ),
        ));
  }
}
