import 'dart:collection';

final class Company extends LinkedListEntry<Company> {
  int empCount;
  String compName;
  double rev;

  Company(this.empCount, this.compName, this.rev);

  String toString() {
    return "$empCount : $compName : $rev";
  }
}

void main() {
  var compInfo = LinkedList<Company>();

  compInfo.add(new Company(700, "Veritas", 1000.0));
  compInfo.add(new Company(1000, "Google", 2000.0));
  compInfo.add(new Company(1500, "Microsoft", 3000.0));

  print(compInfo);
  print(compInfo.first);
}
