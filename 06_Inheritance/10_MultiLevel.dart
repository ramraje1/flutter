class ICC {
  ICC() {
    print("ICC Constructor");
  }
}

class BCCI extends ICC {
  BCCI() {
    print("BCCI Constructor");
  }
}

class IPL extends BCCI {
  IPL() {
    print("IPL Contructor");
  }
}

void main() {
  IPL obj = new IPL();
}
