class Parent {
  int x = 10;
  String str1 = "Ramraje";

  get getX => x;
  get getStr1 => str1;
}

class Child extends Parent {
  int y = 20;
  String str2 = "Bakle";

  get gety => y;
  get getStr2 => str2;
}

void main() {
  Child obj = new Child();

  print(obj.getX);
  print(obj.getStr1);

  print(obj.gety);
  print(obj.getStr2);
}
