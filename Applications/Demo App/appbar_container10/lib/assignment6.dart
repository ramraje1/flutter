//Create a screen in which add 10 colourful containers vertically and make
//the screen scrollable.

//Create 2 containers in the centre of the screen and give that color to the
//containers.

import 'package:flutter/material.dart';

class Assignment6 extends StatefulWidget {
  const Assignment6({super.key});
  @override
  State<Assignment6> createState() => _Assignment6State();
}

class _Assignment6State extends State<Assignment6> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: const Text("Assignment6")),
          actions: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
          ],
          backgroundColor: Colors.purple,
        ),
        body: SingleChildScrollView(
          child: Center(
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
              SizedBox(
                height: 80,
              ),
              Container(
                color: Color.fromARGB(255, 255, 149, 0),
                width: 360,
                height: 200,
              ),
            ]),
          ),
        ));
  }
}
