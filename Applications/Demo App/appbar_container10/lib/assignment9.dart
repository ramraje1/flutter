// In the screen, add a container with size (width: 300,height:300) and add a
// border to that Container, the border must be of color red. Also, add
// rounded corners to the container with a radius 20

import 'package:flutter/material.dart';

class Assignment9 extends StatefulWidget {
  const Assignment9({super.key});
  @override
  State<Assignment9> createState() => _Assignment9State();
}

class _Assignment9State extends State<Assignment9> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
              child: const Text(
            'Assignment9',
            style: TextStyle(color: Colors.black),
          )),
          actions: [
            IconButton(
              color: Colors.black,
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            IconButton(
              color: Colors.black,
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
          ],
          backgroundColor: Color.fromARGB(255, 254, 119, 0),
        ),
        body: Center(
          child: Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
                color: Colors.lime,
                border: Border.all(color: Colors.red, width: 10),
                borderRadius: BorderRadius.all(Radius.circular(20))),
          ),
        ));
  }
}
