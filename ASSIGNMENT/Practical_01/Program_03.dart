//Program 3: Write a program to print all single digit numbers in reverse order

void main() {
  for (int i = 9; i >= 0; i--) {
    print(i);
  }
}
