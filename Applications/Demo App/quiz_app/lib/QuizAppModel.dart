import 'package:flutter/material.dart';

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});
  @override
  State createState() => _QuizAppState();
}

class SingleQuestionModel {
  final String? question;
  final List<String>? options;
  final int? answerIndex;
  const SingleQuestionModel({this.question, this.options, this.answerIndex});
}

class _QuizAppState extends State {
  List allQuestions = [
    const SingleQuestionModel(
      question: "How long is an IPv6 address?",
      options: ["128 bits", "64 bits", "128 bytes", "32 bits"],
      answerIndex: 0,
    ),
    const SingleQuestionModel(
      question:
          "In programming, what do you call functions with the same name but different implementations?",
      options: ["Overriding", "Inheriting", "Abstracting", "Overloading"],
      answerIndex: 3,
    ),
    const SingleQuestionModel(
      question: "_______ is the smallest unit of data in a computer ?",
      options: ["Gigabyte", "Bit", "Byte", "Terabyte"],
      answerIndex: 1,
    ),
    const SingleQuestionModel(
      question: "Which of the following is NOT an anti-virus software ?",
      options: ["Avast", "Linux", "Norton", "Kaspersky"],
      answerIndex: 1,
    ),
    const SingleQuestionModel(
      question: "In the context of computing, a byte is equal to _____ bits ?",
      options: ["4", "16", "24", "8"],
      answerIndex: 3,
    ),
  ];
  bool questionScreen = true;
  int questionIndex = 0;
  int selectedIndex = -1;
  int correctAnswerCount = 0;

  Color? buttonColor(int buttonIndex) {
    if (selectedIndex != -1) {
      if (buttonIndex == allQuestions[questionIndex].answerIndex) {
        return Colors.green;
      } else if (buttonIndex == selectedIndex) {
        return Colors.red;
      } else {
        return Colors.blue;
      }
    } else {
      return Colors.blue;
    }
  }

  void validateCurrentPage() {
    if (selectedIndex == -1) {
      return;
    }
    if (selectedIndex == allQuestions[questionIndex].answerIndex) {
      correctAnswerCount += 1;
    }
    if (selectedIndex != -1) {
      if (questionIndex == allQuestions.length - 1) {
        setState(() {
          questionScreen = false;
        });
      }

      setState(() {
        selectedIndex = -1;
        questionIndex += 1;
      });
    }
  }

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.blue,
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 30,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              const Text(
                "Questions : ",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text(
                "${questionIndex + 1}/${allQuestions.length}",
                style: const TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ]),
            const SizedBox(
              height: 50,
            ),
            Text(
              allQuestions[questionIndex].question,
              style: const TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.w400,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            SizedBox(
              width: 200,
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  if (selectedIndex == -1) {
                    setState(() {
                      selectedIndex = 0;
                    });
                  }
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(buttonColor(0))),
                child: Text(
                  "A.${allQuestions[questionIndex].options[0]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 200,
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  if (selectedIndex == -1) {
                    setState(() {
                      selectedIndex = 1;
                    });
                  }
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(buttonColor(1))),
                child: Text(
                  "B.${allQuestions[questionIndex].options[1]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 200,
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  if (selectedIndex == -1) {
                    setState(() {
                      selectedIndex = 2;
                    });
                  }
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(buttonColor(2))),
                child: Text(
                  "C.${allQuestions[questionIndex].options[2]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            SizedBox(
              width: 200,
              height: 50,
              child: ElevatedButton(
                onPressed: () {
                  if (selectedIndex == -1) {
                    setState(() {
                      selectedIndex = 3;
                    });
                  }
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(buttonColor(3))),
                child: Text(
                  "D.${allQuestions[questionIndex].options[3]}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            validateCurrentPage();
          },
          backgroundColor: Colors.blue,
          child: const Icon(
            Icons.forward,
            color: Colors.white,
          ),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
              color: Colors.white,
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.blue,
        ),
        body: Column(
          children: [
            SizedBox(height: 500, child: Image.asset("assets/images/win.jpg")),
            const Text(
              "Congratulations!!!",
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.w700),
            ),
            const SizedBox(
              height: 10,
            ),
            const Text(
              "You have Completed Quiz",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              "$correctAnswerCount/${allQuestions.length}",
              style: const TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
            ),
            ElevatedButton(
                onPressed: () {
                  questionIndex = 0;
                  questionScreen = true;
                  correctAnswerCount = 0;
                  selectedIndex = -1;
                  setState(() {});
                },
                style: const ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.blue)),
                child: const Text(
                  "RESET",
                  style: TextStyle(color: Colors.black87),
                ))
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
