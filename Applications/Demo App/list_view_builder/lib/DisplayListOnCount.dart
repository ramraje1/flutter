import 'package:flutter/material.dart';

class DisplayCount extends StatefulWidget {
  const DisplayCount({super.key});
  @override
  State createState() => _DisplayCountState();
}

class _DisplayCountState extends State<DisplayCount> {
  List<Widget> container = [];
  int count = 1;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(centerTitle: true, title: Text("Display List On Count")),
      body: ListView.builder(
          itemCount: container.length,
          itemBuilder: (context, index) {
            return container[index];
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            container.add(Container(
              margin: EdgeInsets.all(16),
              padding: const EdgeInsets.only(left: 20),
              alignment: Alignment.centerLeft,
              height: 75,
              width: double.infinity,
              color: Colors.grey,
              child: Text(
                "$count",
                style: TextStyle(
                  fontSize: 32,
                ),
              ),
            ));
            count++;
          });
        },
        child: const Icon(Icons.add),
      ),
    ));
  }
}
