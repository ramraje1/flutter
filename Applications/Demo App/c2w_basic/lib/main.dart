import 'package:c2w_basic/c2w.dart';
import 'package:c2w_basic/c2ww.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: C2W(),
      debugShowCheckedModeBanner: false,
    );
  }
}
