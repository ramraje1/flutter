import 'package:automatic_trading/StockHome.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorSchemeSeed: Colors.purple,
      ),
      home: StockHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
