import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int counter = -1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Portfolio"),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            counter++;
          });
        },
        child: const Text("Next"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                (counter >= 0)
                    ? Container(
                        child: const Text(
                          "Name : Ramraje Ashok Bakle",
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      )
                    : Container(),
                (counter >= 1)
                    ? Container(
                        padding: EdgeInsets.only(bottom: 20),
                        height: 175,
                        width: 200,
                        child: Image.asset("assets/images/ramraje.JPG"),
                      )
                    : Container(),
                (counter >= 2)
                    ? Container(
                        child: const Text(
                          "Collage Name : TSSM's Bhivrabai Sawant College of Engineering",
                          style: TextStyle(fontStyle: FontStyle.italic),
                        ),
                      )
                    : Container(),
                (counter >= 3)
                    ? Container(
                        padding: EdgeInsets.only(bottom: 20),
                        height: 150,
                        width: 150,
                        child: Image.asset("assets/images/clg.jpg"),
                      )
                    : Container(),
                (counter >= 4)
                    ? Container(
                        child: const Text(
                          "Company Name : Biencaps Systems pvt.ltd",
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      )
                    : Container(),
                (counter >= 5)
                    ? Container(
                        height: 190,
                        width: 150,
                        child: Image.asset("assets/images/bi.jpg"),
                      )
                    : Container(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
