/*Program 6: Write a program to print the square of odd digits between 20 to
10
Output: 361 289 225 169 121
*/
void main() {
  for (int i = 20; i >= 10; i--) {
    if (i % 2 != 0) {
      print(i * i);
    }
  }
}
