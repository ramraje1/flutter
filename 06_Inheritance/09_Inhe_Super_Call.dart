class Parent {
  //Base, SuperClass , Parent

  Parent() {
    print("Parent Constructor");
  }
  void call() {
    print("In Call Method");
  }
}

class Child extends Parent {
  //Derived , SubClass , Child

  Child() {
    super();
    print("Child Constructor");
  }
}

void main() {
  Child obj = new Child();
  obj();
}
