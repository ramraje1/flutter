abstract class IFC {
  void material() {
    print("Indian Material");
  }

  void teste();
}

class IndianFC implements IFC {
  void material() {
    print("Indian Material");
  }

  void teste() {
    print("Indian teste");
  }
}

class EUFC implements IFC {
  void material() {
    print("Indian Material");
  }

  void teste() {
    print("Eropian teste");
  }
}

void main() {
  IndianFC obj = new IndianFC();
  obj.material();
  obj.teste();
}
