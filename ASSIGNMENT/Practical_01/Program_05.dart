/*Program 5: Write a program to print the square of even digits between
40 to 50
Output: 1600 1764 1936 2116 2304 2500*/
void main() {
  for (int i = 40; i <=50; i=i+2) {
    print(i*i);
  }
}
