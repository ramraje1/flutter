import 'package:automatic_trading/LoginPage.dart';
import 'package:flutter/material.dart';

// A StatefulWidget that creates the forgot password page
class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

// A State object that holds the state of the forgot password page
class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  // Text controller for the text field
  final TextEditingController _emailController = TextEditingController();

  // A form key to access and validate the form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // A bool variable to indicate whether the password reset process is in progress or not
  bool _isLoading = false;

  // A method to handle the password reset logic
  void _resetPassword() async {
    // Validate the form
    if (_formKey.currentState!.validate()) {
      // Set the loading indicator to true
      setState(() {
        _isLoading = true;
      });
      // Get the input value
      String email = _emailController.text.trim();
      // TODO: Add your password reset logic here, such as sending a password reset email to the user and showing a confirmation message
      // For example, you can use Firebase Authentication to send the password reset email
      // try {
      //   await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      //   // Show a confirmation message
      //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('A password reset link has been sent to $email')));
      // } on FirebaseAuthException catch (e) {
      //   // Handle the password reset error
      //   // For example, you can show a snackbar with the error message
      //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.message!)));
      // }
      // Set the loading indicator to false
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Forgot Password'),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              const SizedBox(height: 16),
              const Text(
                'Forgot Password',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.all(16),
                child: TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    // Validate the email input
                    if (value!.isEmpty) {
                      return 'Please enter your email';
                    }
                    if (!value.contains('@')) {
                      return 'Please enter a valid email';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: _isLoading
                    ? const CircularProgressIndicator()
                    : ElevatedButton(
                        onPressed: _resetPassword,
                        child: const Text('Reset Password'),
                      ),
              ),
              TextButton(
                onPressed: () {
                  // Navigate to the login page
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()));
                },
                child: const Text('Back to Login'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
