import 'package:flutter/material.dart';

void main() {
  runApp(IndianFlagApp());
}

class IndianFlagApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Indian Flag with Stand'),
        ),
        body: Center(
          child: Container(
            height: 300.0, // Adjust the height according to your need
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                // Vertical stand
                Container(
                  width: 20.0,
                  color: Colors.brown,
                ),
                SizedBox(width: 10.0), // Space between the stand and flag
                // Indian flag
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      width: 150.0,
                      height: 100.0,
                      color: Colors.orange,
                    ),
                    Container(
                      width: 150.0,
                      height: 100.0,
                      color: Colors.white,
                    ),
                    Container(
                      width: 150.0,
                      height: 100.0,
                      color: Colors.green,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
