/*Program 7: Write a program to print the square of odd digits and cube of
even digits between 40 to 50
Output: 64000 1681 74088 1849 85184 2025 97336 2209 110592
2401 125000
*/
void main() {
  for (int i = 40; i <=50; i=i+2) {
    print(i*i*i);
  }
}