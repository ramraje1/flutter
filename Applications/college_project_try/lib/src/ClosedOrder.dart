import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

// A StatelessWidget that creates the order page
class ClosedOrderPage extends StatelessWidget {
  ClosedOrderPage({Key? key}) : super(key: key);
  String date = DateFormat('dd-MM-yyyy').format(DateTime.now());
  // String date = DateTime.now().toString();

  // @override
  // Widget build(BuildContext context) {
  //   return DataTable(
  //     columns: const [
  //       DataColumn(label: Text('#')),
  //       DataColumn(label: Text('Exchange')),
  //       DataColumn(label: Text('Instrument Type')),
  //       DataColumn(label: Text('Symbol')),
  //       DataColumn(label: Text('Product Type')),
  //       DataColumn(label: Text('Order Type')),
  //       DataColumn(label: Text('Quantity')),
  //       DataColumn(label: Text('BUY/SELL')),
  //       DataColumn(label: Text('Target')),
  //       DataColumn(label: Text('SL')),
  //       //DataColumn(label: Text('Date')),
  //     ],
  //     rows: [
  //       // TODO: Add your data rows here, each containing a list of DataCell widgets
  //       // For example, you can use Text widgets to display the data values
  //       DataRow(cells: [
  //         DataCell(Text('1')),
  //         DataCell(Text('NSE')),
  //         DataCell(Text('Reliance')),
  //         DataCell(Text('10')),
  //         DataCell(Text('2882.37')),
  //         DataCell(Text('MIS')),
  //         DataCell(Text('Buy')),
  //         DataCell(Text('2900.40')),
  //         DataCell(Text('2830.83')),
  //         DataCell(Text("$date")),
  //       ]),
  //       DataRow(cells: [
  //         DataCell(Text('2')),
  //         DataCell(Text('BSE')),
  //         DataCell(Text('TATA STEEL')),
  //         DataCell(Text('20')),
  //         DataCell(Text('142.56')),
  //         DataCell(Text('NRML')),
  //         DataCell(Text('Sell')),
  //         DataCell(Text('120.10')),
  //         DataCell(Text('160.29')),
  //         DataCell(Text('$date')),
  //       ]),
  //     ],
  //   );

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: const [
        DataColumn(label: Text('#')),
        DataColumn(label: Text('Exchange')),
        DataColumn(label: Text('Instrument Type')),
        DataColumn(label: Text('Symbol')),
        DataColumn(label: Text('Product Type')),
        DataColumn(label: Text('Order Type')),
        DataColumn(label: Text('Quantity')),
        DataColumn(label: Text('BUY/SELL')),
        DataColumn(label: Text('Target')),
        DataColumn(label: Text('SL')),
        //DataColumn(label: Text('Date')),
      ],
      rows: [
        // TODO: Add your data rows here, each containing a list of DataCell widgets
        // For example, you can use Text widgets to display the data values
        DataRow(cells: [
          DataCell(Text('1')),
          DataCell(Text('NSE')),
          DataCell(Text('EQTY')),
          DataCell(Text('Reliance')),
          DataCell(Text('MIS')),
          DataCell(Text('Market')),
          DataCell(Text('10')),
          DataCell(Text(
            'Buy',
            style: TextStyle(color: Colors.green),
          )),
          DataCell(Text('2900.40')),
          DataCell(Text('2830.83')),
          //DataCell(Text("$date")),
        ]),

        DataRow(cells: [
          DataCell(Text('2')),
          DataCell(Text('BSE')),
          DataCell(Text('NFO')),
          DataCell(Text('TATA STEEL')),
          DataCell(Text('Limit')),
          DataCell(Text('NRML')),
          DataCell(Text('20')),
          DataCell(Text(
            'Sell',
            style: TextStyle(color: Colors.red),
          )),
          DataCell(Text('120.10')),
          DataCell(Text('160.29')),
          //DataCell(Text('$date')),
        ]),
      ],
    );
  }
}
