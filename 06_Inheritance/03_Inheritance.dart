class Parent {
  int x = 10;
  String str1 = "Parent Name";

  void parentMethod() {
    print(x);
    print(str1);
  }
}

class Child extends Parent {
  int y = 20;
  String str2 = "Data";
  void ChildMethod() {
    print(y);
    print(str2);
  }
}

void main() {
  Parent obj = new Parent();
  print(obj.y);
  print(obj.str2);
  obj.ChildMethod();
}
