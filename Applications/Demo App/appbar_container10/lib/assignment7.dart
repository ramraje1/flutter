//Create a screen in which add 5 network images horizontally with size
//(width: 150, height: 300) and make scrollable.

import 'package:flutter/material.dart';

class Assignment7 extends StatefulWidget {
  const Assignment7({super.key});
  @override
  State<Assignment7> createState() => _Assignment7State();
}

class _Assignment7State extends State<Assignment7> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(child: const Text("Assignment7")),
          actions: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
          ],
          backgroundColor: Colors.purple,
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Center(
            child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Image.network(
                  "https://t3.ftcdn.net/jpg/03/31/59/64/360_F_331596496_IBES7GleA7yyj4GCRnbXTsHu0SdJW40f.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://t3.ftcdn.net/jpg/03/31/59/64/360_F_331596496_IBES7GleA7yyj4GCRnbXTsHu0SdJW40f.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://e0.pxfuel.com/wallpapers/1009/824/desktop-wallpaper-ram-mandir-ram-mandir-ayodhya-for-thumbnail.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://i.pinimg.com/736x/cb/73/b5/cb73b58b68a22c553b69ea291d4d21e9.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://t3.ftcdn.net/jpg/03/31/59/64/360_F_331596496_IBES7GleA7yyj4GCRnbXTsHu0SdJW40f.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://e0.pxfuel.com/wallpapers/1009/824/desktop-wallpaper-ram-mandir-ram-mandir-ayodhya-for-thumbnail.jpg"),
              SizedBox(
                width: 80,
              ),
              Image.network(
                  "https://i.pinimg.com/736x/cb/73/b5/cb73b58b68a22c553b69ea291d4d21e9.jpg")
            ]),
          ),
        ));
  }
}
