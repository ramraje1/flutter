// In the screen, add a container with size (width: 300,height:300). Give
// colour to the container. Make container’s top-left and bottom-right
// corners rounded with radius 20

import 'package:flutter/material.dart';

class Assignment10 extends StatefulWidget {
  const Assignment10({super.key});
  @override
  State<Assignment10> createState() => _Assignment10State();
}

class _Assignment10State extends State<Assignment10> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
              child: const Text(
            'Assignment10',
            style: TextStyle(color: Colors.black),
          )),
          actions: [
            IconButton(
              color: Colors.black,
              icon: Icon(Icons.home),
              onPressed: () {},
            ),
            IconButton(
              color: Colors.black,
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
          ],
          backgroundColor: Color.fromARGB(255, 254, 119, 0),
        ),
        body: Center(
          child: Container(
            width: 300,
            height: 300,
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 254, 246, 0),
                border: Border.all(color: Colors.red, width: 20),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20))
                //BorderRadius.all(Radius.circular(20))
                ),
          ),
        ));
  }
}
