// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';

// // A StatefulWidget that creates the symbol setting page
// class SettingPage extends StatefulWidget {
//   const SettingPage({Key? key}) : super(key: key);

//   @override
//   _SettingPageState createState() => _SettingPageState();
// }

// class _SettingPageState extends State<SettingPage> {
//   // Declare the variables and controllers that you need to store the data and inputs of the symbol setting page
//   String _name = 'Ramraje Bakle';
//   String _email = "ramrajebakle@gmail.com";
//   int _phone = 9860371929;
//   String _address = 'JSPM Narhe, Pune';
//   String _brokerName = 'KITE';
//   String _brokerLoginId = "kite123ram";
//   String _password = '123456';
//   String _apiKey = "abc123";
//   bool _switchValue = true;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: const Text('Add/Edit Symbol'),
//         ),
//         body: Center(
//           child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: [
//                       Text("Name : $_name"),
//                       Text("Email : $_email"),
//                       Text("Phone No. :  $_phone"),
//                       Text("Address :  $_address"),
//                       SizedBox(height: 10),
//                       Text("Broker Name :  $_brokerName"),
//                       Text("Login ID :  $_brokerLoginId"),
//                       Text("Password  :  $_brokerLoginId"),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Text("2FA : "),
//                           CupertinoSwitch(
//                             value: _switchValue,
//                             onChanged: (value) {
//                               setState(() {
//                                 _switchValue = value;
//                               });
//                             },
//                           ),
//                         ],
//                       ),
//                     ]),
//                 SizedBox(
//                   width: 40,
//                 )
//               ]),
//         ));
//   }
// }

import 'package:flutter/material.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  // Create text editing controllers for each field
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final addressController = TextEditingController();
  final brokerNameController = TextEditingController();
  final loginIdController = TextEditingController();
  final passwordController = TextEditingController();

  // Create a focus node for each field
  final nameFocus = FocusNode();
  final emailFocus = FocusNode();
  final phoneFocus = FocusNode();
  final addressFocus = FocusNode();
  final brokerNameFocus = FocusNode();
  final loginIdFocus = FocusNode();
  final passwordFocus = FocusNode();

  // Create a boolean variable for the 2FA switch
  bool is2FAEnabled = false;

  // Create a boolean variable for the edit mode
  bool isEditing = false;

  @override
  void dispose() {
    // Dispose the controllers and focus nodes when the widget is removed
    nameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    addressController.dispose();
    brokerNameController.dispose();
    loginIdController.dispose();
    passwordController.dispose();
    nameFocus.dispose();
    emailFocus.dispose();
    phoneFocus.dispose();
    addressFocus.dispose();
    brokerNameFocus.dispose();
    loginIdFocus.dispose();
    passwordFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Setting Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Create a text field for name
            TextField(
              controller: nameController,
              focusNode: nameFocus,
              decoration: InputDecoration(
                labelText: 'Name',
                border: OutlineInputBorder(),
              ),
              // Enable or disable the text field based on the edit mode
              enabled: isEditing,
            ),
            SizedBox(height: 8.0),
            // Create a text field for email
            TextField(
              controller: emailController,
              focusNode: emailFocus,
              decoration: InputDecoration(
                labelText: 'Email',
                border: OutlineInputBorder(),
              ),
              // Enable or disable the text field based on the edit mode
              enabled: isEditing,
            ),
            SizedBox(height: 8.0),
            // Create a text field for phone
            TextField(
              controller: phoneController,
              focusNode: phoneFocus,
              decoration: InputDecoration(
                labelText: 'Phone',
                border: OutlineInputBorder(),
              ),
              // Enable or disable the text field based on the edit mode
              enabled: isEditing,
            ),
            SizedBox(height: 8.0),
            // Create a text field for address
            TextField(
              controller: addressController,
              focusNode: addressFocus,
              decoration: InputDecoration(
                labelText: 'Address',
                border: OutlineInputBorder(),
              ),
              // Enable or disable the text field based on the edit mode
              enabled: isEditing,
            ),
            SizedBox(height: 8.0),
            // Create a text field for broker name
            TextField(
              controller: brokerNameController,
              focusNode: brokerNameFocus,
              decoration: InputDecoration(
                labelText: 'Broker Name',
                border: OutlineInputBorder(),
              ),
              // Enable or disable the text field based on the edit mode
              enabled: isEditing,
            ),
            SizedBox(height: 8.0),
            // Create a text field for login id
            TextField(
              controller: loginIdController,
              focusNode: loginIdFocus,
              decoration: InputDecoration(
                labelText: 'Login Id',
                border: OutlineInputBorder(),
              ),
              // Enable or disable the text field based on the edit mode
              enabled: isEditing,
            ),
            SizedBox(height: 8.0),
            // Create a text field for password
            TextField(
              controller: passwordController,
              focusNode: passwordFocus,
              decoration: InputDecoration(
                labelText: 'Password',
                border: OutlineInputBorder(),
              ),
              // Enable or disable the text field based on the edit mode
              enabled: isEditing,
              // Obscure the password text
              obscureText: true,
            ),
            SizedBox(height: 8.0),
            // Create a switch list tile for 2FA
            SwitchListTile(
              title: Text('2FA'),
              value: is2FAEnabled,
              onChanged: (value) {
                // Update the state of the switch
                setState(() {
                  is2FAEnabled = value;
                });
              },
              // Enable or disable the switch based on the edit mode
              activeColor: isEditing ? Colors.red : Colors.red,
              inactiveThumbColor: isEditing ? Colors.green : Colors.green,
            ),
            Spacer(),
            // Create a center widget for the save and edit button
            Center(
              child: ElevatedButton(
                child: Text(isEditing ? 'Save' : 'Edit'),
                onPressed: () {
                  // Toggle the edit mode
                  setState(() {
                    isEditing = !isEditing;
                  });
                  // If the user pressed save, validate and submit the data
                  if (!isEditing) {
                    // TODO: Add your validation and submission logic here
                  }
                },
              ),
            ),
            SizedBox(height: 16.0),
          ],
        ),
      ),
    );
  }
}
