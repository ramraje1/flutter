import '16_factory.dart';

void main() {
  BackEnd obj1 = new BackEnd(("Java Script"));

  BackEnd obj2 = new BackEnd("Java");

  BackEnd obj3 = new BackEnd("Python");

  print(obj1.lang);
  print(obj2.lang);
  print(obj3.lang);
}
