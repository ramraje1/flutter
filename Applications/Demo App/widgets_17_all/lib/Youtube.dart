import 'package:flutter/material.dart';

class Youtube extends StatefulWidget {
  const Youtube({super.key});
  @override
  State<Youtube> createState() => _YoutubeState();
}

class _YoutubeState extends State<Youtube> {
  List<String> imageList = [
    "https://i.ytimg.com/vi/km45w9JZy04/maxresdefault.jpg"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey,
        title: const Text(
          "YouTube",
          // selectionColor: Colors.black,
        ),
        actions: [
          const Icon(Icons.video_call),
          IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
          Icon(Icons.circle)
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.red,
        child: const Icon(Icons.stop),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              const Text(
                "History",
                style: TextStyle(fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                height: 10,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(children: [
                  Container(
                    height: 120,
                    width: 200,
                    child: Image.network(
                        "https://i.ytimg.com/vi/nU5oWfUWEGo/maxresdefault.jpg"),
                  ),
                  Container(
                    height: 120,
                    width: 200,
                    child: Image.network(
                        "https://i.ytimg.com/vi/1VGOghLPX78/sddefault.jpg"),
                  ),
                  Container(
                    height: 120,
                    width: 200,
                    child: Image.network(
                        "https://media.licdn.com/dms/image/D5612AQEvuK-23omyIA/article-cover_image-shrink_720_1280/0/1676103952310?e=2147483647&v=beta&t=JJ8Q-FW4WoR5nAxuXKsM_WLhfrP2CJSfWAqQIPuyTDQ"),
                  ),
                ]),
              ),
              Row(
                children: [
                  Container(
                    height: 250,
                    width: 410,
                    child: Image.network(
                        "https://i.ytimg.com/vi/xrR3xQNeB_Y/maxresdefault.jpg"),
                  )
                ],
              ),
              const Text(
                  "Sasta Shark Tank ep:125,Pehle toh sabse bahut saari maafiya itna late lekin ab se puri ..."),
              ElevatedButton(
                  onPressed: () {}, child: const Center(child: Text("Play"))),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 250,
                    width: 410,
                    child: Image.network(
                        "https://c4.wallpaperflare.com/wallpaper/711/338/415/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg"),
                  )
                ],
              ),
              const Text("rolls royce hd wallpapers 1080p download for pc"),
              ElevatedButton(
                  onPressed: () {}, child: const Center(child: Text("Play"))),
            ],
          ),
        ],
      ),
    );
  }
}
