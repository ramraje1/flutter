abstract class Devloper {
  void develop() {
    print("We build Software");
  }

  void devType();
}

class Mobaile extends Devloper {
  void devType() {
    print("Flutter Dev");
  }
}

class WebDev extends Devloper {
  void devType() {
    print("FrontEnd Dev");
  }
}

void main() {
  Devloper obj = new Mobaile();
  obj.develop();
  obj.devType();

  Devloper obj1 = new WebDev();
  obj1.develop();
  obj1.devType();

  WebDev obj2 = new WebDev();
  obj2.develop();
  obj2.devType();
}
