import 'package:flutter/material.dart';
//import 'QuizAppUI1.dart';
import 'QuizAppUI2.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: "Quiz App",
      home: QuizAppUI2(),
      debugShowCheckedModeBanner: false,
    );
  }
}
