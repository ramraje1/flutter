abstract class InterfaceDemo1 {
  void m1() {
    print("In m1 Interface");
  }
}

abstract class InterfaceDemo2 {
  void m2() {
    print("In m2 Interface");
  }
}

class Demo implements InterfaceDemo1, InterfaceDemo2 {
  void m1() {
    print("In m1 Interface");
  }

  void m2() {
    print("In m2 Interface");
  }
}

void main() {
  Demo obj = new Demo();

  obj.m1();
  obj.m2();
}
