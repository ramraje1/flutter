class Demo {
  int? _x;
  String? str;
  double? _sal;

  Demo(this._x, this.str, this._sal);

  get getX => _x;
  double? get getSal => _sal;

  get getStr {
    return str;
  }
}

void main() {
  Demo obj = new Demo(10, "Ramraje", 1.4);

  print(obj.getX);
  print(obj.str);
  print(obj.getSal);
}
