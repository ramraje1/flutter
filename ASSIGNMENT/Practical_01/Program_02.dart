//Program 2 : Write a program to print a table of 5 in reverse order

void main() {
  int n = 5;

  for (int i = 10; i >= 1; i--) {
    print(i * n);
  }
}
