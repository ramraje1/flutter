/*Program 8: Write a program to print the product of odd digits between 10
to 1
Output: 945
 */
void main() {
  var pro = 1;
  for (int i = 10; i >= 1; i--) {
    if (i % 2 != 0) {
      pro = pro * i;
    }
  }
  print(pro);
}
