import 'assignment1.dart';
import 'assignment2.dart';
import 'assignment3.dart';
import 'assignment4.dart';
import 'assignment5.dart';
import 'assignment6.dart';
import 'assignment7.dart';
import 'assignment8.dart';
import 'assignment9.dart';
import 'assignment10.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      //home: Assignment1(),
      //home: Assignment2(),
      // home: Assignment3(),
      //home: Assignment4(),
      home: Assignment5(),
      //home: Assignment6(),
      //home: Assignment7(),
      //home: Assignment8(),
      //home: Assignment9(),
      //home: Assignment10(),

      debugShowCheckedModeBanner: false,
    );
  }
}
