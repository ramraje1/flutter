import 'dart:collection';

void main() {
  var compData = Queue();

  compData.add("Microsoft");
  compData.add("Amozon");
  compData.add("Google");

  print(compData.runtimeType);

  print(compData);

  compData.addFirst("Apple");
  compData.addLast("Veritas");

  print(compData);

  compData.removeLast();

  print(compData);
}
