class BackEnd {
  String? lang;

  BackEnd._code(String lang) {
    if (lang == 'Java Script')
      this.lang = 'NodeJs';
    else if (lang == 'Java')
      this.lang = "SpringBoot";
    else
      this.lang = "NodeJs/SpringBoot";
  }
  factory BackEnd(String lang) {
    return BackEnd._code(lang);
  }
}
