import 'package:flutter/material.dart';

class ImageOnClick extends StatefulWidget {
  const ImageOnClick({super.key});
  @override
  State createState() => _ImageListState();
}

class _ImageListState extends State<ImageOnClick> {
  int index = 0;
  List<String> images = [
    "https://c4.wallpaperflare.com/wallpaper/402/508/992/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/711/338/415/rolls-royce-wraith-rolls-royce-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/37/495/765/rolls-royce-dawn-rolls-royce-2018-cars-4k-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/538/280/3/rolls-royce-dawn-overdose-rolls-royce-2017-cars-hd-wallpaper-preview.jpg",
    "https://c4.wallpaperflare.com/wallpaper/339/381/65/rolls-royce-dawn-overdose-rolls-royce-2017-cars-hd-wallpaper-preview.jpg"
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(centerTitle: true, title: Text("Display Image")),
      body: Container(
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.centerLeft,
          color: Colors.orange,
          child: Image.network(images[index])),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            index++;
            if (index >= images.length) {
              index = 0;
            }
          });
        },
        child: const Icon(Icons.navigate_next),
      ),
    ));
  }
}
