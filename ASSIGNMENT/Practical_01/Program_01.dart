//Program 1: Write a program to print a table of 2
void main() {
  for (var i = 1; i <= 10; i++) {
    print(i * 2);
  }
}
