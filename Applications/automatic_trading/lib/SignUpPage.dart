import 'package:flutter/material.dart';
import 'LoginPage.dart';

// A StatefulWidget that creates the create account page
class CreateAccountPage extends StatefulWidget {
  const CreateAccountPage({Key? key}) : super(key: key);

  @override
  _CreateAccountPageState createState() => _CreateAccountPageState();
}

// A State object that holds the state of the create account page
class _CreateAccountPageState extends State<CreateAccountPage> {
  // Text controllers for the text fields
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();

  // A form key to access and validate the form
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // A bool variable to indicate whether the create account process is in progress or not
  bool _isLoading = false;

  // A method to handle the create account logic
  void _createAccount() async {
    // Validate the form
    if (_formKey.currentState!.validate()) {
      // Set the loading indicator to true
      setState(() {
        _isLoading = true;
      });
      // Get the input values
      String name = _nameController.text.trim();
      String email = _emailController.text.trim();
      String password = _passwordController.text.trim();
      // TODO: Add your create account logic here, such as creating the user account and navigating to the home page
      // For example, you can use Firebase Authentication to create the user account
      // try {
      //   UserCredential userCredential =
      //       await FirebaseAuth.instance.createUserWithEmailAndPassword(
      //     email: email,
      //     password: password,
      //   );
      //   // Update the user display name
      //   await userCredential.user!.updateDisplayName(name);
      //   // Navigate to the home page
      //   Navigator.pushReplacement(
      //       context, MaterialPageRoute(builder: (context) => LoginPage()));
      // } on FirebaseAuthException catch (e) {
      //   // Handle the create account error
      //   // For example, you can show a snackbar with the error message
      //   ScaffoldMessenger.of(context)
      //       .showSnackBar(SnackBar(content: Text(e.message!)));
      // }
      // Set the loading indicator to false
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Create Account'),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              const SizedBox(height: 16),
              const Text(
                'Create Account',
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.all(16),
                child: TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(
                    labelText: 'Name',
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    // Validate the name input
                    if (value!.isEmpty) {
                      return 'Please enter your name';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(
                    labelText: 'Email',
                    border: OutlineInputBorder(),
                  ),
                  validator: (value) {
                    // Validate the email input
                    if (value!.isEmpty) {
                      return 'Please enter your email';
                    }
                    if (!value.contains('@')) {
                      return 'Please enter a valid email';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: TextFormField(
                  controller: _passwordController,
                  decoration: const InputDecoration(
                    labelText: 'Password',
                    border: OutlineInputBorder(),
                  ),
                  obscureText: true,
                  validator: (value) {
                    // Validate the password input
                    if (value!.isEmpty) {
                      return 'Please enter your password';
                    }
                    if (value.length < 6) {
                      return 'Please enter a password with at least 6 characters';
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: TextFormField(
                  controller: _confirmPasswordController,
                  decoration: const InputDecoration(
                    labelText: 'Confirm Password',
                    border: OutlineInputBorder(),
                  ),
                  obscureText: true,
                  validator: (value) {
                    // Validate the confirm password input
                    if (value!.isEmpty) {
                      return 'Please confirm your password';
                    }
                    if (value != _passwordController.text) {
                      return 'Passwords do not match';
                    }
                    return null;
                  },
                ),
              ),
              TextButton(
                onPressed: () {
                  // Navigate to the login page
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const LoginPage()));
                },
                child: const Text('Already have an account? Login'),
              ),
              Padding(
                padding: const EdgeInsets.all(16),
                child: _isLoading
                    ? const CircularProgressIndicator()
                    : ElevatedButton(
                        onPressed: _createAccount,
                        child: const Text('Create Account'),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
