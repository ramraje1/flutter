class Employee {
  int empId = 37;
  String name = "Ramraje";
  double sal = 1.24;

  void empInfo() {
    print(empId);
    print(name);
    print(sal);
  }
}

void main() {
  Employee obj1 = new Employee();
  obj1.empInfo();

  Employee obj2 = Employee();
  obj2.empInfo();

  print("===============================================");

  obj1.sal = 1.75;
  obj1.empInfo();
  obj2.empInfo();
}
