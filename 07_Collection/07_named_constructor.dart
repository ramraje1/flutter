class Demo {
  Demo() {
    print("Named Constructor");
  }
  Demo.one() {
    print("Named one Constructor");
  }
  Demo.Two() {
    print("Named Two Constructor");
  }
}

void main() {
  Demo obj1 = new Demo();
  Demo obj2 = new Demo.one();
  Demo obj3 = new Demo.Two();
}
